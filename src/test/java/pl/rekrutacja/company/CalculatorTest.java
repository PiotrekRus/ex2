package pl.rekrutacja.company;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

public class CalculatorTest {
    private final RandomIntSupplier integerMaxSupplier = () -> Optional.of(Integer.MAX_VALUE);
    private final RandomIntSupplier optionalEmptySupplier = Optional::empty;

    @Test
    public void shouldReturnResultWhenSumTwoIntMax() {
//        given: "calculator with two Integer.MAX_VALUE suppliers"
        var calculator = new Calculator(integerMaxSupplier, integerMaxSupplier);
//        when: "call sum integers"
        var result = calculator.sumIntegers();
//        then: "expect result is 4294967294"
        Assert.assertEquals(Long.valueOf("4294967294"), result);
    }

    @Test
    public void shouldReturnIntegerMaxWhenSupplyIntegerMaxAndOptionalEmpty() {
//        given: "calculator with Integer.MAX_VALUE and Optional.Empty suppliers"
        var calculator = new Calculator(integerMaxSupplier, optionalEmptySupplier);
//        when: "call sum integers"
        var result = calculator.sumIntegers();
//        then: "expect result is 4294967294"
        Assert.assertEquals(Long.valueOf(Integer.MAX_VALUE), result);
    }

    @Test
    public void shouldReturnZeroWhenSupplyOptionalNullAndOptionalNull() {
//        given: "calculator with two Optional.Empty suppliers"
        var calculator = new Calculator(optionalEmptySupplier, optionalEmptySupplier);
//        when: "call sum integers"
        var result = calculator.sumIntegers();
//        then: "expect result is 4294967294"
        Assert.assertEquals(Long.valueOf("0"), result);
    }

}
