package pl.rekrutacja.company;

import pl.rekrutacja.company.database_client.DatabaseSupplier;
import pl.rekrutacja.company.http_client.HttpSupplier;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        final var calculator = new Calculator(new HttpSupplier(), new DatabaseSupplier());
        var sum = calculator.sumIntegers();
        logger.log(Level.INFO, "Database int + http int = {0}", sum);
    }


}
