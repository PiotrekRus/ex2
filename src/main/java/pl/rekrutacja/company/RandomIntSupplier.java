package pl.rekrutacja.company;

import java.util.Optional;

public interface RandomIntSupplier {
    Optional<Integer> getRandomInt();
}
