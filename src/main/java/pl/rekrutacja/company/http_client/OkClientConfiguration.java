package pl.rekrutacja.company.http_client;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;

class OkClientConfiguration {
    static final String RANDOM_ORG_API = "https://www.random.org/integers/?num=1&min=-1000000000&max=1000000000&col=1&base=10&format=plain&rnd=new";

    private static OkHttpClient getOkHttpClient() {
        return new OkHttpClient();
    }

    private static Request setRequest() {
        return new Request.Builder()
                .url(RANDOM_ORG_API)
                .build();
    }

    static Call getHttpCall() {
        return getOkHttpClient().newCall(setRequest());
    }
}
