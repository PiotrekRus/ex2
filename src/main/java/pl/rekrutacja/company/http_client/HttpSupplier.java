package pl.rekrutacja.company.http_client;

import okhttp3.Response;
import pl.rekrutacja.company.RandomIntSupplier;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpSupplier implements RandomIntSupplier {
    static final String OK_CODE = "200";
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public Optional<Integer> getRandomInt() {
        return callApi();
    }

    private Optional<Integer> callApi() {
        try {
            var response = OkClientConfiguration.getHttpCall().execute();
            return parseResponse(response);
        } catch (IOException e) {
            logger.log(Level.WARNING, "Exception: {0}", e.getMessage());
        } catch (NumberFormatException e) {
            logger.log(Level.WARNING, "Can not parse response: {0}", e.getMessage());
        }
        return Optional.empty();
    }

    private Optional<Integer> parseResponse(Response response) throws IOException, NumberFormatException {
        if (Integer.valueOf(OK_CODE).equals(response.code())) {
            final var value = Integer.valueOf(response.body().string().trim());
            logger.log(Level.INFO, "Http int fetched: {0}", value);
            return Optional.of(value);
        }
        logger.log(Level.INFO, "API status different than OK, response: {0}", response);
        return Optional.empty();
    }
}

