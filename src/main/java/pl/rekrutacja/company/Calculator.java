package pl.rekrutacja.company;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

class Calculator {
    private final RandomIntSupplier first;
    private final RandomIntSupplier second;

    public Calculator(RandomIntSupplier first, RandomIntSupplier second) {
        this.first = first;
        this.second = second;
    }

    public Long sumIntegers() {
        CompletableFuture<Optional<Integer>> futureFirst = CompletableFuture.supplyAsync(first::getRandomInt);
        CompletableFuture<Optional<Integer>> futureSecond = CompletableFuture.supplyAsync(second::getRandomInt);

        return Stream.of(futureSecond, futureFirst)
                .map(CompletableFuture::join)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .mapToLong(Integer::intValue)
                .sum();
    }
}
