package pl.rekrutacja.company.database_client;

import pl.rekrutacja.company.RandomIntSupplier;

import java.util.Optional;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseSupplier implements RandomIntSupplier {

    private final Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public Optional<Integer> getRandomInt() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        final var randomInt = new Random().nextInt();
        logger.log(Level.INFO, "Database int fetched: {0}", randomInt);
        return Optional.of(randomInt);
    }
}
