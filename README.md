# README #
### Goals
Uprzejmie proszę o wykonanie prostej funkcji, której zadaniem jest dodanie 2 liczb. Jedna z nich niech będzie pobierana z hipotetycznego API dostępnego online (można wykorzystać np. https://www.random.org/clients/http/api/), źródło drugiej może być dowolne (Math.random(), pobierana z hipotetycznej bazy danych itp).

W zadaniu chodzi głównie o pokazanie stylu i sposobu kodowania (dekompozycja kodu, podział funkcji itp). Można użyć mockowych źródeł danych, nie muszą być zaimplementowane wszystkie przypadki brzegowe (ale warto je mieć z tyłu głowy). Kod ten będzie służył jako punkt wyjścia podczas rozmowy rekrutacyjnej.

Ważne też, żeby kod napisany był w sposób, jaki uważa Pan za najlepszy pod względem designu i stylu.

### What is this repository for? ###

 Recruitment application. It fetches one integer from www.random.org API, and the second from a database. In the end, it sums them up and prints result in a console.

### Tolls required ###
* JDK11
* Maven
### How do I run it up? ###

* clone repo
* make sure command "java --version" prints java 11, if not, set the correct java version
* in root run command "mvn clean install"
* run command "java -jar target/company-rekrutacja-1.0-SNAPSHOT-jar-with-dependencies.jar"

